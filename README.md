# Docker Workshop
Lab 03: Updating and Sharing Containers

---

## Overview

In this lab we will learn how to create your own images based on running containers by using docker commit. Then you will learn how to use docker save and docker load to share your images using tarball files. In the first part we will use docker exec to create a "secret file" inside a running container and then we will use docker commit to create your own image (the base image + the secret file). Finally, we will convert our image into a file by using docker save and then we will convert the image file into a image using docker load (after clean your docker host).


## Preparations

 - Create a new folder for the lab:
```
$ mkdir ~/lab-03
$ cd ~/lab-03
```

## Docker Commit

 - Run the application (version 3.0) in a Docker container (detached mode) in the port 5000 using:
```
$ docker run -d -p 5000:3000 --name static-app-3.0 selaworkshops/npm-static-app:3.0
```

 - Ensure the container is running:
```
$ docker ps
```

 - Create a new file into the container using the commands:
```
$ docker exec -it static-app-3.0 /bin/bash
```
```
$ echo "Very secret content :)" > secret-file
```
```
$ ls -l
```
```
$ cat secret-file
```
```
$ exit
```

 - Check the current images:
```
$ docker images
```

 - Create a new image including the changes (name it mycustomimage:3.0):
```
$ docker commit static-app-3.0 customimage:3.0
```

 - Check the current images:
```
$ docker images
```

 - Delete the container static-app-3.0 using:
```
$ docker rm -f static-app-3.0
```

 - Run a new container from the version 3 image (in port 5000):
```
$ docker run -d -p 5000:3000 --name static-app-3.0 selaworkshops/npm-static-app:3.0
```

 - Run a container from the image you created using docker commit (in port 5001):
```
$ docker run -d -p 5001:3000 --name custom-app-3.0 customimage:3.0
```

 - Inspect the running containers with:
```
$ docker ps
```

 - Inspect the filesystem of the version 3.0 container:
```
$ docker exec static-app-3.0 ls -l
```

 - Inspect the filesystem of the commited container:
```
$ docker exec custom-app-3.0 ls -l
```

 - As you see images doesn't change when a container changes but you can "commit" new images to save the container changes 

 
 
## Docker Save

 - Now let's save the commited container in a file and store it in the current directory (may take a while):
```
$ docker save -o custom-app-3.0.tar customimage:3.0
```

 - Ensure the image was created in the current directory:
```
$ ls
```


## Docker Load

 - Clean your docker host environment:
```
$ docker rm -f $(docker ps -a -q)
```
```
$ docker rmi -f $(docker images -a -q)
```

 - Ensure that your docker host environment is clean:
```
$ docker ps
```
```
$ docker images
```

 - Let's load the docker image from the created file:
```
$ docker load -i custom-app-3.0.tar
```

 - Ensure the image was loaded successfully:
 ```
 $ docker images
 ```
 
 - Run a container from the loaded image:
```
$ docker run -d -p 3000:3000 --name custom-app-3.0 customimage:3.0
```

 - Inspect the filesystem of the commited container:
```
$ docker exec custom-app-3.0 ls -l
```


## Cleanup

 - Delete the container
```
docker rm -f custom-app-3.0
```

 - Remove the images
```
docker rmi customimage:3.0
```